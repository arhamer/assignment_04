function  [eigens,percent] = analyze_MD(inputType, inputs, database_name, user_name, password)
%Analyze market data by sector or by list of ticket names.
%   analyze_MD(inputType, inputs, database_name, user_name, password)
%   inputType = "Sector" or "Ticker"
%   inputs = sector analayzed ('Technology') or array of bloomberg ticker
%   names (example: ["AAPL";"GE";"KO";"VLO"])
%   database_name = name of the ODBC matlab is connected to
%   user_name, password = the credential used for ODBC connection ('' can be used for password) 
 
timeseries = retrieveData(inputType, inputs, database_name, user_name, password);
[eigens, percent] = evalue(timeseries);

disp('The matrix of eigenvalues is:')
format shortG
disp(eigens)

mess = sprintf('The proportion of variation accounted for by the largest eignevalue is %.4f',percent);
disp(mess)
end

%query data
%Query by sector or QUery by ticker
function [data] = retrieveData(iType, input, database_name, user_name, password)
    %%%% create SQL string pointing to MarketData database
    
    % Set preferences
    prefs = setdbprefs('DataReturnFormat');
    setdbprefs('DataReturnFormat','table')

    % Make connection to database
    conn = database(database_name,user_name,password);
    
    %stock_ID's is going to be a list of stockID.
    %we need to make a for loop from 1:length(stock_IDS) that fill a column
    %in a matrix for each stock_ID
    if(iType == 'Sector')

        % input is sector name
        query_sectorID = ['SELECT Sector_ID ' ...
            'FROM MarketData.dbo.MarketData_SectorInfo '...
            sprintf('WHERE Sector_Name = ''%s''', input)];
        query_sectorID = fetch(conn, query_sectorID);
        sector_ID = table2array(query_sectorID);
        %close(query_sectorID);
        
        %query stock ticker
        curs = exec(conn, ['SELECT Ticker ' ...
            'FROM MarketData.dbo.MarketData_Info '...
            sprintf('WHERE Sector = %.0f', sector_ID)]);
        curs = fetch(curs);
        ticker = curs.Data;
        dp = ticker.Variables;
        me = sprintf('%s, ', dp{:});
        disp('The stocks analyzed are:')
        disp(me)
        
        query_stockIDs = ['SELECT StockID ' ...
            'FROM MarketData.dbo.MarketData_Info '...
            sprintf('WHERE Sector = %.0f', sector_ID)];
        query_stockIDs = fetch(conn, query_stockIDs);
        stock_IDs = table2array(query_stockIDs);
        %close(query_stockIDs);        
        
        query_EOD = ['SELECT EODPrice ' ...
            'FROM MarketData.dbo.EODPrice_Values '];
        for c = 1:length(stock_IDs)
            query_data = [query_EOD, ...
                sprintf('WHERE StockID = %.0f', stock_IDs(c))];
            query_data = fetch(conn, query_data);
            data(:,c) = table2array(query_data);
        end 
    elseif(iType == 'Ticker')
        %display the stock ticker
        me = sprintf('%s, ', input{:});
        disp('The stocks analyzed are:')
        disp(me)
        
        %input is an array of BBG tickers
        query_stocks = ['SELECT StockID ' ...
            'FROM MarketData.dbo.MarketData_Info '];
        for c = 1:length(input)
            query_stockIDs = [query_stocks, ...
                sprintf('WHERE Ticker = ''%s''', input(c))];
            query_stockIDs = fetch(conn, query_stockIDs);
            stock_IDs(c) = table2array(query_stockIDs);
        end 
        
       query_EOD = ['SELECT EODPrice ' ...
            'FROM MarketData.dbo.EODPrice_Values '];
        for c = 1:length(stock_IDs)
            query_data = [query_EOD, ...
                sprintf('WHERE StockID = %.0f ', stock_IDs(c))];             
            query_data = fetch(conn, query_data);
            data(:,c) = table2array(query_data);
        end 
    else
        fprintf('Incorrect inputType');
        data = 0;
    end
    
    close(conn);
end

%calculate the eigenvalue of the matrix
function [ ematr,ple ] = evalue( matrix )
    [vec,val] = eig(cov(stockreturn(matrix)));
    
    ematr = val; % Eigenvalue value matrix
    
    a = diag(val); %vector with all eigenvalues
    su = sum(a);
    
    % calc what % of total is attributed to each eigen value
    perc = a(:)/su; %% of the variation accounted for by each eigenvalue

    %
    ple = perc(length(perc));
end

%calculate the log return for the stock price matrix
function [ result ] = stockreturn( spmatrix )
    [or,oc] = size(spmatrix);
    rmatrix = log(spmatrix(2:end,:))-log(spmatrix(1:end-1,:));
    
    for a = 1:oc   
        rmatrix(:,a) = rmatrix(:,a)- mean(rmatrix(:,a));
    end    
    result = rmatrix;
end