function [result] = adddata (filename)
td = xlsread(filename);
conn = database('mdb','Li','');
column = {'EntryID';'StockID';'EODPrice'};
for c = 1:length(td)
    colval = {c,td(c,1),td(c,3)};
    fastinsert (conn,'MarketData.dbo.EODPrice_Values', column,colval);
end
close(conn)
result = "Done";
end
