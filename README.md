# README #

FM 5091 - Assignment 04

Those who worked on this project include:

Meron Tefera, 
David Rokhinson, 
Li Zhang, 
Alexander Hamer

Instructions:

Restore SQL database from backup using the uploaded .bak file. 

Analyze market data by sector or by list of ticket names.

analyze_MD(inputType, inputs, database_name, user_name, password)

inputType = "Sector" or "Ticker"

inputs = sector analayzed ('Technology') or array of bloomberg ticker names (example: ["AAPL";"GE";"KO";"VLO"])

database_name = name of the ODBC matlab is connected to

user_name, password = the credential used for ODBC connection ('' can be used for password) 
