%Date range of the stock price is 2/22/2017 - 11/1/2017
%Data source is Yahoo Finance

%PCA analysis for four stocks within the technology sector
[sm,sp] = analyze_MD('Sector','Technology','mdb','Li','');

%PCA analsysis for four stocks from four different sector
slist = ["GOOG";"GE";"KO";"VLO"];
[tm,tp] = analyze_MD('Ticker',slist,'mdb','Li','');